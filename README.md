# Statistics Library

This is a statistics library that provides various statistical calculations and exports the results in different formats.

## Features

- Calculation of various statistics, including average number of cyclists per day, total number of days, most frequently traveled section, and total number of cyclists per day.
- Exporting statistics to XML and JSON file formats.

## Usage

- This app is used by providing 3 arguments in this order
   - file name
     - filename including extension like "data.csv"
   - statistics shortcuts
     - CP = celkový počet cyklistů za den
     - ED = celkový počet cyklistů za každý den
     - NU = nejčastěji projíždený úsek
     - PD = celkový počet dnů 
     - PP = průměrný počet cyklistů za den
   - export shortcuts
     - XML = export do XML
     - JSON = export do JSON
