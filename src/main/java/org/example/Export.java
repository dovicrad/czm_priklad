package org.example;

import java.util.Set;

/**
 * Abstract class for exporting statistics.
 * Provides a skeleton used for saving statistics into a file.
 */
public abstract class Export {
    protected static final String path = "src/main/resources/";
    protected static Set<Statistic> statistics;

    /**
     * Constructor for the Export class.
     *
     * @param statistics The set of statistics to be exported.
     */
    public Export(Set<Statistic> statistics) {
        Export.statistics = statistics;
    }

    /**
     * Abstract method for saving the statistics.
     * Implementations of this method will define the specific export format and logic.
     */
    protected abstract void save();
}
