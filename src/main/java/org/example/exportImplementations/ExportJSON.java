package org.example.exportImplementations;

import org.example.Export;
import org.example.NestedStatistic;
import org.example.Statistic;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.util.Set;

/**
 * Implementation of exporting statistics to a JSON file.
 */
public class ExportJSON extends Export {
    /**
     * Constructs an ExportJSON object with the given set of statistics.
     *
     * @param statistics The set of statistics to be exported.
     */
    public ExportJSON(Set<Statistic> statistics) {
        super(statistics);
    }

    /**
     * Saves the statistics to a JSON file.
     */
    @Override
    protected void save() {
        try {
            FileWriter file = new FileWriter(path + "json_export-" + System.currentTimeMillis() + ".json");
            file.write(getJSON().toString(4));
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Generates the JSON object for the statistics.
     *
     * @return The generated JSON object.
     */
    public JSONObject getJSON() {
        JSONObject report = new JSONObject();
        report.put("statistics", getStatisticsResults(statistics));
        report.put("created", java.time.LocalDateTime.now().toString());
        return report;
    }

    /**
     * Creates a JSON array with the statistics objects.
     *
     * @param statistics The set of statistics to convert to a JSON array.
     * @return The JSON array of statistics objects.
     */
    private JSONArray getStatisticsResults(Set<Statistic> statistics) {
        JSONArray statisticsResults = new JSONArray();
        for (Statistic statistic : statistics) {
            statisticsResults.put(getStatistic(statistic));
        }
        return statisticsResults;
    }

    /**
     * Creates a JSON object with the name and result fields for a statistic.
     *
     * @param statistic The statistic object to convert to a JSON object.
     * @return The JSON object representing the statistic.
     */
    private JSONObject getStatistic(Statistic statistic) {
        JSONObject statistics = new JSONObject();
        statistics.put("name", statistic.getResult().getName());
        if (statistic instanceof NestedStatistic) {
            statistics.put("statistics", getStatisticsResults(((NestedStatistic) statistic).getStatistics()));
        } else {
            statistics.put("result", statistic.getResult().getResult());
        }
        return statistics;
    }
}
