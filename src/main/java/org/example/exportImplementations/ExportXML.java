package org.example.exportImplementations;

import org.example.Export;
import org.example.NestedStatistic;
import org.example.Statistic;
import org.example.StatisticResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Set;

/**
 * Implementation of exporting statistics to an XML file.
 */
public class ExportXML extends Export {
    /**
     * Constructs an ExportXML object with the given set of statistics.
     *
     * @param statistics The set of statistics to be exported.
     */
    public ExportXML(Set<Statistic> statistics) {
        super(statistics);
    }

    /**
     * Saves the statistics to an XML file.
     */
    @Override
    protected void save() {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            File file = new File(path + "xml_export-" + System.currentTimeMillis() + ".xml");
            transformer.transform(new DOMSource(getXML()), new StreamResult(file));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Generates the XML document for the statistics.
     *
     * @return The generated XML document.
     */
    public Document getXML() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.newDocument();
            doc.appendChild(createReportElement(doc));

            return doc;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates the report element with the created and statisticsResults subelements.
     *
     * @param doc The XML document to create the element in.
     * @return The created report element.
     */
    private Element createReportElement(Document doc) {
        Element report = doc.createElement("report");
        report.appendChild(createCreatedElement(doc));
        report.appendChild(createStatisticsResultsElement(doc));
        return report;
    }

    /**
     * Creates the created element with the current date and time as text content.
     *
     * @param doc The XML document to create the element in.
     * @return The created element.
     */
    private Element createCreatedElement(Document doc) {
        Element created = doc.createElement("created");
        created.setTextContent(java.time.LocalDateTime.now().toString());
        return created;
    }

    /**
     * Creates the statisticsResults element with the statistics elements as children.
     *
     * @param doc The XML document to create the element in.
     * @return The created statisticsResults element.
     */
    private Element createStatisticsResultsElement(Document doc) {
        Element statisticsResults = doc.createElement("statisticsResults");
        for (Statistic statistic : statistics) {
            statisticsResults.appendChild(createStatisticElement(doc, statistic));
        }
        return statisticsResults;
    }

    /**
     * Creates a statistic element with the name and result subelements.
     *
     * @param doc       The XML document to create the element in.
     * @param statistic The statistic object to create the element for.
     * @return The created statistic element.
     */
    private Element createStatisticElement(Document doc, Statistic statistic) {
        Element statistics = doc.createElement("statistics");
        statistics.appendChild(createNameElement(doc, statistic.getResult()));
        statistics.appendChild(createResultElement(doc, statistic));
        return statistics;
    }

    /**
     * Creates a name element with the statistic name as text content.
     *
     * @param doc             The XML document to create the element in.
     * @param statisticResult The statistic result object to retrieve the name from.
     * @return The created name element.
     */
    private Element createNameElement(Document doc, StatisticResult statisticResult) {
        Element name = doc.createElement("name");
        name.setTextContent(statisticResult.getName());
        return name;
    }

    /**
     * Creates a result element with the statistic value as text content.
     * For nested statistics, recursively creates statistic elements for each nested statistic.
     *
     * @param doc       The XML document to create the element in.
     * @param statistic The statistic object to create the element for.
     * @return The created result element.
     */
    private Element createResultElement(Document doc, Statistic statistic) {
        Element result = doc.createElement("result");
        if (statistic instanceof NestedStatistic) {
            for (Statistic stat : ((NestedStatistic) statistic).getStatistics()) {
                result.appendChild(createStatisticElement(doc, stat));
            }
        } else {
            result.setTextContent(statistic.getResult().getResult());
        }
        return result;
    }
}
