package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for parsing input files into a 2D list of Strings.
 */
public class InputFileParser {
    public static final String CSV_EXTENSION = "csv";

    /**
     * Parses the input file into a 2D list.
     * Considers possibility of adding additional formats
     *
     * @param fileName The name of the input file.
     * @return A 2D list representing the parsed data from the input file.
     */
    public List<List<String>> parseIntoArray(String fileName){
        String fileExtension = fileName.substring(fileName.lastIndexOf('.')+1);
        Path filePath = Path.of("src/main/resources", fileName);
        try{
            return switch (fileExtension) {
                case CSV_EXTENSION -> this.parseIntoArrayCSV(filePath);
                default -> throw new IOException("ERROR: unsupported file type");
            };
        } catch (IOException e) {
            throw new RuntimeException("ERROR: while reading data:\n" + e);
        }
    }

    /**
     * Parses a CSV file into a 2D list.
     *
     * @param filePath The path of the CSV file.
     * @return A 2D list representing the parsed data from the CSV file.
     * @throws IOException If an I/O error occurs while reading the file.
     */
    public List<List<String>> parseIntoArrayCSV(Path filePath) throws IOException {
        try (Stream<String> lines = Files.lines(filePath)) {
            return lines
                .skip(1)
                .map(line -> line.split(","))
                .map(values -> {
                    if (values.length < 5) {
                        values = Arrays.copyOf(values, 5);
                        Arrays.fill(values, 4, 5, "0");
                    }
                    return List.of(values);
                }).collect(Collectors.toList());
        }
    }
}
