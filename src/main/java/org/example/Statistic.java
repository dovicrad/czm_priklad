package org.example;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Skeleton for every available statistic.
 * Calculates statistics based on a given dataset.
 */
public abstract class Statistic {
    protected static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE_TIME;
    protected static List<List<String>> data;
    protected StatisticResult result;

    /**
     * Constructor for the Statistic class.
     *
     * @param data The dataset to be used for calculating the statistic.
     */
    public Statistic(List<List<String>> data){
        Statistic.data = data;
    }

    /**
     * Abstract method for calculating the statistic.
     *
     * @return The result of the statistic calculation.
     */
    protected abstract StatisticResult calculate();

    /**
     * Retrieves the result of the statistic calculation.
     * If the result has not been calculated yet, it will be calculated first.
     *
     * @return The result of the statistic calculation.
     */
    public StatisticResult getResult(){
        if(result == null){
            this.result = this.calculate();
        }
        return this.result;
    }

    Set<LocalDate> daysSet = new HashSet<>();
    public Set<LocalDate> getSetOfDays(){
        if(daysSet.isEmpty()){
            for(List<String> record : data){
                LocalDate fromDate = LocalDate.parse(record.get(2), FORMATTER);
                LocalDate toDate = LocalDate.parse(record.get(3), FORMATTER);
                daysSet.add(fromDate);
                daysSet.add(toDate);
            }
        }
        return daysSet;
    }
}
