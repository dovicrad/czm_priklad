package org.example;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Abstract class for nested statistics.
 * Extends the Statistic class and provides a set of nested statistics.
 */
public abstract class NestedStatistic extends Statistic {

    protected Set<Statistic> statistics = new HashSet<>();

    public NestedStatistic(List<List<String>> data) {
        super(data);
    }

    /**
     * Abstract method for calculating the nested statistic.
     *
     * @return The result of the nested statistic calculation.
     */
    @Override
    protected abstract StatisticResult calculate();

    /**
     * Retrieves the set of nested statistics.
     *
     * @return A set of nested statistics.
     */
    public Set<Statistic> getStatistics() {
        return statistics;
    }
}

