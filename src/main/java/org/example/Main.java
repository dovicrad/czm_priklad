package org.example;

import org.example.exportImplementations.ExportJSON;
import org.example.exportImplementations.ExportXML;
import org.example.statisticImplementations.*;

import java.util.*;

/**
 * Main class to execute the program.
 * Parses input arguments, calculates selected statistics, and exports the results.
 */
public class Main {
    public static void main(String[] args) {
        // Check if the number of program arguments is 3
        if (args.length != 3){
            throw new RuntimeException("ERROR: incorrect count of program arguments");
        }

        // Extract the program arguments
        String fileName = args[0];
        String[] toBeExecutedStatisticsNames = args[1].split(",");
        String[] toBeExecutedExportNames = args[2].split(",");

        // Create an instance of InputFileParser and parse the input file into a 2D list
        InputFileParser parser = new InputFileParser();
        List<List<String>> array = parser.parseIntoArray(fileName);

        // Create a map to hold the available statistics
        Map<String, Statistic> statisticMap = Map.of(
                "PP", new StatisticPP(array),
                "CP", new StatisticCP(array),
                "PD", new StatisticPD(array),
                "NU", new StatisticNU(array),
                "ED", new StatisticED(array)
        );

        // Create a set to hold the selected statistics based on the input arguments
        Set<Statistic> statistics = new HashSet<>();
        for (String statisticName : toBeExecutedStatisticsNames) {
            // Get the statistic object from the map based on the statistic name
            Statistic statistic = statisticMap.get(statisticName);
            if (statistic == null) {
                System.err.println("Statistic not found for key: " + statisticName);
            } else {
                // Add the statistic to the set
                statistics.add(statistic);
            }
        }

        // Create a map to hold the available export options
        Map<String, Export> exportMap = Map.of(
                "XML", new ExportXML(statistics),
                "JSON", new ExportJSON(statistics)
        );

        // Create a set to hold the selected export options based on the input arguments
        Set<Export> exports = new HashSet<>();
        for (String exportName : toBeExecutedExportNames) {
            Export export = exportMap.get(exportName);
            // Get the export object from the map based on the export name
            if(export == null){
                System.err.println("Export not found for key: " + exportName);
            }else{
                exports.add(export);
            }
        }

        // Save each export
        for (Export export : exports) {
            export.save();
        }
    }
}
