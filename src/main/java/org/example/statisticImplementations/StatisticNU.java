package org.example.statisticImplementations;

import org.example.Statistic;
import org.example.StatisticResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of a statistic for calculating the most frequently traveled section.
 */
public class StatisticNU extends Statistic {
    String name = "nejčastěji projíždený úsek";

    /**
     * Constructs a StatisticNU object with the given data.
     *
     * @param data The data used for calculating the statistic.
     */
    public StatisticNU(List<List<String>> data) {
        super(data);
    }

    /**
     * Calculates the most frequently traveled section based on the data.
     *
     * @return The result of the statistic calculation.
     */
    @Override
    protected StatisticResult calculate() {
        Map<String, Integer> mapOfLocations = getMapOfLocations();
        String highestLocation = "";
        int highestLocationValue = 0;
        for (var entry : mapOfLocations.entrySet()) {
            int locationValue = entry.getValue();
            if (locationValue > highestLocationValue) {
                highestLocationValue = locationValue;
                highestLocation = entry.getKey();
            }
        }

        // Create and return the statistic result
        return new StatisticResult(name, String.valueOf(highestLocation));
    }

    /**
     * Creates a map of locations and their corresponding values.
     *
     * @return The map of locations and their values.
     */
    protected Map<String, Integer> getMapOfLocations() {
        Map<String, Integer> map = new HashMap<>();
        for (List<String> record : data) {
            int valueOfRecord = Integer.parseInt(record.get(4));
            String location = record.get(0);
            map.merge(location, valueOfRecord, Integer::sum);
        }
        return map;
    }
}
