package org.example.statisticImplementations;

import org.example.Statistic;
import org.example.StatisticResult;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of a statistic for calculating the average number of cyclists per day.
 */
public class StatisticPP extends Statistic {
    String name = "průměrný počet cyklistů za den";

    /**
     * Constructs a StatisticPP object with the given data.
     *
     * @param data The data used for calculating the statistic.
     */
    public StatisticPP(List<List<String>> data) {
        super(data);
    }

    /**
     * Calculates the average number of cyclists per day based on the data.
     *
     * @return The result of the statistic calculation.
     */
    @Override
    protected StatisticResult calculate() {
        Set<LocalDate> dates = getSetOfDays();
        int sum = 0;
        for(List<String> record : data){
            sum += Integer.parseInt(record.get(4));
        }

        // Calculate the average number of cyclists per day
        int average = sum / dates.size();

        // Create and return the statistic result
        return new StatisticResult(name, String.valueOf(average));
    }
}
