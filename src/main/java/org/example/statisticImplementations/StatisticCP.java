package org.example.statisticImplementations;

import org.example.Statistic;
import org.example.StatisticResult;

import java.util.List;

/**
 * Implementation of a statistic for calculating the total number of cyclists per day.
 */
public class StatisticCP extends Statistic {
    String name = "celkový počet cyklistů za všechny dny";

    /**
     * Constructs a StatisticCP object with the given data.
     *
     * @param data The data used for calculating the statistic.
     */
    public StatisticCP(List<List<String>> data) {
        super(data);
    }

    /**
     * Calculates the total number of cyclists per day based on the data.
     *
     * @return The result of the statistic calculation.
     */
    @Override
    protected StatisticResult calculate() {
        int sum = 0;
        for (List<String> record : data) {
            sum += Integer.parseInt(record.get(4));
        }

        // Create and return the statistic result
        return new StatisticResult(name, String.valueOf(sum));
    }
}
