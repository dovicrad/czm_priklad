package org.example.statisticImplementations;

import org.example.NestedStatistic;
import org.example.StatisticResult;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of a nested statistic for calculating the total number of cyclists for each day.
 */
public class StatisticED extends NestedStatistic {
    String name = "celkový počet cyklistů za každý den";

    /**
     * Constructs a StatisticED object with the given data.
     *
     * @param data The data used for calculating the statistic.
     */
    public StatisticED(List<List<String>> data) {
        super(data);
    }

    /**
     * Calculates the total number of cyclists for each day based on the data.
     *
     * @return The result of the statistic calculation.
     */
    @Override
    protected StatisticResult calculate() {
        Map<LocalDate, Integer> map = getMapOfDays();
        for (var entry : map.entrySet()) {
            this.statistics.add(new StatisticSetted(data, "day-" + entry.getKey(), String.valueOf(entry.getValue())));
        }

        // Create and return the statistic result
        return new StatisticResult(name, "");
    }

    /**
     * Retrieves a map of days and sum of their values from the dataset.
     * Only records with positive values are considered.
     * If any record has been recorded in span of multiple days it is ignored
     *
     * @return A map of LocalDate objects as keys and their corresponding values as integers.
     */
    protected Map<LocalDate, Integer> getMapOfDays() {
        Map<LocalDate, Integer> map = new HashMap<>();

        for (List<String> record : data) {
            int valueOfRecord = Integer.parseInt(record.get(4));
            LocalDate fromDate = LocalDate.parse(record.get(2), FORMATTER);
            LocalDate toDate = LocalDate.parse(record.get(3), FORMATTER);
            if(fromDate.isEqual(toDate)){
                map.merge(fromDate, valueOfRecord, Integer::sum);
            }
        }
        return map;
    }
}
