package org.example.statisticImplementations;

import org.example.Statistic;
import org.example.StatisticResult;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Implementation of a statistic for calculating the total number of days.
 */
public class StatisticPD extends Statistic {
    String name = "celkový počet dnů";

    /**
     * Constructs a StatisticPD object with the given data.
     *
     * @param data The data used for calculating the statistic.
     */
    public StatisticPD(List<List<String>> data) {
        super(data);
    }

    /**
     * Calculates the total number of days based on the data.
     *
     * @return The result of the statistic calculation.
     */
    @Override
    protected StatisticResult calculate() {
        Set<LocalDate> dates = getSetOfDays();
        int totalDays = dates.size();

        // Create and return the statistic result
        return new StatisticResult(name, String.valueOf(totalDays));
    }
}
