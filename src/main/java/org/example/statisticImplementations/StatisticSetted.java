package org.example.statisticImplementations;

import org.example.Statistic;
import org.example.StatisticResult;

import java.util.List;

/**
 * Implementation of a statistic with a pre-set result.
 * This statistic is only set in the constructor and used by nested statistics.
 */
public class StatisticSetted extends Statistic {

    public StatisticSetted(List<List<String>> data, String name, String result) {
        super(data);
        this.result = new StatisticResult(name, result);
    }

    @Override
    protected StatisticResult calculate() {
        return result;
    }
}