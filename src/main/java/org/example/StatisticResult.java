package org.example;

/**
 * simple read-only pair implementation
 */
public class StatisticResult {
    private String name;
    private String result;

    /**
     *
     * @param name = name of statistic
     * @param result = result of statistic
     */
    public StatisticResult(String name, String result){
        this.name = name;
        this.result = result;
    }

    public String getName(){
        return name;
    }

    public String getResult(){
        return result;
    }
}
